# Voici la version finale du code 
class File:
    def __init__(self):
        self.items = []

    def enqueue(self, item):
        priority, data = item
        inserted = False

        for i in range(len(self.items)):

            if priority < self.items[i][0]:
                self.items.insert(i, item)
                inserted = True
                break

        if not inserted:
            self.items.append(item)

    def dequeue(self):
        if not self.is_empty():
            return self.items.pop(0)
        else:
            return "La file d'attente est vide :( "


    def is_empty(self):
        return len(self.items) == 0

    def size(self):
        return len(self.items)


file = File()
file.enqueue((4, "Nathan"))
file.enqueue((2, "Julia"))
file.enqueue((1, "Amandine"))
file.enqueue((3, "Mathias"))

print(file.dequeue())
print(file.dequeue())
print(file.dequeue())
print(file.dequeue())
print(file.dequeue()) # On ajoute un 5eme print pour voir si il va afficher le message du return de la methode dequeue

